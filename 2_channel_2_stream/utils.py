import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd


def one_hot_encode(labels):
    return pd.get_dummies(pd.Series(labels)).values


def get_central_parts(images):
    return images[:, 4:12, 4:12, :]


def load_data(patches_file, labels_file, val_size=500, test_size=10000, mode='train'):
    X = np.load(patches_file)
    y = np.load(labels_file)

    if mode == 'train':
        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=val_size, shuffle=True)
        return X_train, X_val, one_hot_encode(y_train), one_hot_encode(y_val)

    elif mode == 'test':
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, shuffle=True)
        return X_test, one_hot_encode(y_test)


def get_batch(patches, labels, num):
    patches, _, labels, _ = train_test_split(patches, labels, train_size=num, shuffle=True)
    return patches, labels


