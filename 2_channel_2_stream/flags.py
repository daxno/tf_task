import tensorflow as tf


def define_flags():
    tf.app.flags.DEFINE_integer('batch_size', 128, 'size of training batches')
    tf.app.flags.DEFINE_float('learning_rate', 1e-3, 'learning rate')
    tf.app.flags.DEFINE_integer('num_iter', 5000, 'number of training iterations')
    tf.app.flags.DEFINE_string('checkpoint_file_path', './checkpoints/', 'path to checkpoint file')
    tf.app.flags.DEFINE_string('summary_dir', './graphs', 'path to directory for storing summaries')
    tf.app.flags.DEFINE_integer('image_size', 16, 'size of image')
    tf.app.flags.DEFINE_integer('image_depth', 6, 'depth of image')
    tf.app.flags.DEFINE_integer('num_labels', 2, 'number of labels')
    tf.app.flags.DEFINE_string('train_labels', './data/train_labels.npy', 'path to train labels')
    tf.app.flags.DEFINE_string('test_labels', './data/test_labels.npy', 'path to test labels')
    tf.app.flags.DEFINE_string('train_patches', './data/train_patches.npy', 'path to train patches')
    tf.app.flags.DEFINE_string('test_patches', './data/test_patches.npy', 'path to test patches')
    tf.app.flags.DEFINE_integer('test_size', 20000, 'test size')