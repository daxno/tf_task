import tensorflow as tf
from model import Model
from utils import load_data, get_batch
from flags import define_flags


def train():
    model = Model(learning_rate=FLAGS.learning_rate, num_labels=FLAGS.num_labels)

    with tf.Graph().as_default():
        images, val_images, labels, val_labels = load_data(labels_file=FLAGS.train_labels,
                                                           patches_file=FLAGS.train_patches,
                                                           val_size=10000, mode='train')

        x = tf.placeholder(shape=[None, FLAGS.image_size, FLAGS.image_size, FLAGS.image_depth], dtype=tf.float32,
                           name='x')
        y = tf.placeholder(shape=[None, FLAGS.num_labels], dtype=tf.int64, name='y')

        keep_prob = tf.placeholder(tf.float32, name='dropout_prob')
        global_step = tf.contrib.framework.get_or_create_global_step()

        logits = model.inference(x, keep_prob=keep_prob)

        accuracy = model.accuracy(logits, y)
        f1_score = model.f1_score(logits, y)
        conf_mat = model.confusion_matrix(logits, y)
        loss = model.loss(logits, y)

        summary_op = tf.summary.merge_all()
        train_op = model.train(loss, global_step=global_step)

        init = tf.global_variables_initializer()
        saver = tf.train.Saver()

        with tf.Session(config=tf.ConfigProto(log_device_placement=False)) as sess:
            writer = tf.summary.FileWriter(FLAGS.summary_dir, sess.graph)
            sess.run(init)
            for i in range(FLAGS.num_iter):

                batch_x, batch_y = get_batch(images, labels, FLAGS.batch_size)

                _, cur_loss, summary = sess.run([train_op, loss, summary_op],
                                                feed_dict={x: batch_x, y: batch_y, keep_prob: 0.5})
                writer.add_summary(summary, i)
                if i % 100 == 0:
                    print(f'Iter {i} Loss: {cur_loss}')
                if i % 1000 == 0 or i == FLAGS.num_iter-1:
                    validation_accuracy = accuracy.eval(feed_dict={x: val_images,
                                                                   y: val_labels, keep_prob: 1.0})
                    validation_f1 = f1_score.eval(feed_dict={x: val_images,
                                                             y: val_labels, keep_prob: 1.0})
                    conf_matrix = conf_mat.eval(feed_dict={x: val_images,
                                                             y: val_labels, keep_prob: 1.0})
                    print(f'Iter {i} Accuracy: {validation_accuracy}')
                    print(f'Iter {i} F1: {validation_f1}')
                    print(f'Confusion matrix\n{conf_matrix}')

                if i == FLAGS.num_iter - 1:
                    saver.save(sess, FLAGS.checkpoint_file_path)


def main(argv=None):
    train()


if __name__ == '__main__':
    FLAGS = tf.app.flags.FLAGS
    define_flags()

    tf.app.run()