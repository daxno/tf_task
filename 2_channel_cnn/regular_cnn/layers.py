import tensorflow as tf


# def create_fully_connected(X, shape, units):
#     with tf.variable_scope('fc') as scope:
#         reshape = tf.reshape(X, [-1, shape])
#         W_fc1 = create_weights([shape, units])
#         b_fc1 = create_bias([units])
#         fc = tf.nn.relu(tf.matmul(reshape, W_fc1) + b_fc1, name=scope.name)
#     return fc
#
#
# def create_conv2d(X, ksize, strides=[1, 1, 1, 1], pad='SAME'):
#     with tf.variable_scope('conv') as scope:
#         W = create_weights(ksize)
#         bias = create_bias([ksize[3]])
#         conv = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(input=X, filter=W, strides=strides, padding=pad), bias),
#                           name=scope.name)
#     return conv
#
#     # return tf.nn.conv2d(input=X,
#     #                     filter=W,
#     #                     strides=strides,
#     #                     padding=pad)
#
#
# def create_max_pool(X, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], pad='SAME'):
#     return tf.nn.max_pool(value=X,
#                           ksize=ksize,
#                           strides=strides,
#                           padding=pad)
#
#
# def create_weights(shape):
#     return tf.Variable(tf.truncated_normal(shape=shape, stddev=0.1, dtype=tf.float32))
#
#
# def create_bias(shape):
#     return tf.Variable(tf.constant(1., shape=shape, dtype=tf.float32))