import tensorflow as tf

from model import Model
from utils import load_data
from flags import define_flags

FLAGS = tf.app.flags.FLAGS


def evaluate():
    with tf.Graph().as_default():
        images, labels = load_data(labels_file=FLAGS.test_labels, patches_file=FLAGS.test_patches, mode='test',
                                   test_size=FLAGS.test_size)
        model = Model(learning_rate=FLAGS.learning_rate, num_labels=FLAGS.num_labels)

        logits = model.inference(tf.cast(images, dtype=tf.float32), keep_prob=1.0)
        accuracy = model.accuracy(logits, tf.cast(labels, dtype=tf.int64))
        f1_score = model.f1_score(logits, tf.cast(labels, dtype=tf.int64))

        saver = tf.train.Saver()

        with tf.Session(config=tf.ConfigProto(log_device_placement=False)) as sess:

            tf.global_variables_initializer().run()
            saver.restore(sess, FLAGS.checkpoint_file_path)

            total_accuracy, total_f1 = sess.run([accuracy, f1_score])
            print(f'Test accuracy: {total_accuracy}')
            print(f'Test f1 score: {total_f1}')


def main(argv=None):
    evaluate()


if __name__ == '__main__':
    FLAGS = tf.app.flags.FLAGS
    define_flags()

    tf.app.run()