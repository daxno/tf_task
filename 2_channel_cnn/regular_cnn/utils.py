import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd


def one_hot_encode(labels):
    return pd.get_dummies(pd.Series(labels)).values


def load_data(patches_file, labels_file, val_size=500, test_size=10000, mode='train'):
    X = np.load(patches_file)
    y = np.load(labels_file)

    if mode == 'train':
        X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=val_size, shuffle=True)
        return X_train, X_val, one_hot_encode(y_train), one_hot_encode(y_val)

    elif mode == 'test':
        idx = np.arange(len(X))
        np.random.shuffle(idx)
        test_idx = idx[:test_size]
        return X[test_idx], one_hot_encode(y[idx[:test_size]])


def get_batch(patches, labels, num):
    idx = np.arange(0 , len(patches))
    np.random.shuffle(idx)
    idx = idx[:num]
    patches_shuffled = patches[idx]
    labels_shuffled = labels[idx]
    return patches_shuffled, labels_shuffled


